# Abstract #

castNcrew provides an online platform for connecting models and directors directly.


# Version #


This is the first prototype of our product.
In this version we provide an interface for models to register their accounts and login to their corresponding profiles.

# How to use it #
1. At first, run createdb.py initialise the database
2. Then run app.py run the project
3. This starts the development web server listening at port 5050
4. Visit localhost:5050 or 127.0.0.1:5050 in the browser to use the app.

### Who do I talk to? ###

Read contributors.txt