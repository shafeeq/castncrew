import os

DEBUG = True

DATA_FOLDER = os.environ['OPENSHIFT_DATA_DIR']
SQLALCHEMY_DATABASE_URI = os.environ['OPENSHIFT_POSTGRESQL_DB_URL'] + 'castncrew'
SECRET_KEY = "this is some kind of super secret"
