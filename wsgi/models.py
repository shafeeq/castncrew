from werkzeug import generate_password_hash
import datetime

from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = "users"
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String, nullable=False)
    lastname = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique = True)
    mobilenumber = db.Column(db.String, nullable=False)
    alternate_contact = db.Column(db.String, nullable = False)
    pwdhash = db.Column(db.String, nullable=False)
    location = db.Column(db.String)
    gender = db.Column(db.String)
    registered_on = db.Column(db.DateTime)

    authenticated = db.Column(db.Boolean, default=False)
    is_admin = db.Column(db.Boolean, default = False)
    verified = db.Column(db.Boolean, default = False)
    user_role = db.Column(db.String, nullable=False)


    __mapper_args__ = {
        'polymorphic_identity':'user',
        'polymorphic_on':user_role,
        'with_polymorphic':'*'
    }


    def __init__(self, firstname, lastname, email, password, mobno, gender, location, alternate_contact,role='model'):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.mobilenumber = mobno
        self.gender = gender
        self.alternate_contact = alternate_contact
        self.location = location
        self.pwdhash = generate_password_hash(password)
        self.registered_on = datetime.datetime.utcnow()
        self.user_role = role

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False

    def __repr__(self):
        return '<id: %r - email: %r - role:%r>' %(self.id, self.email, self.user_role)

 


class Artist(User):
    """The base class model for the models derived from User """
    __tablename__ = "artists"
    id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)

    __mapper_args__ = {
        'polymorphic_identity':'artist',
    }

    city_and_state = db.Column(db.String)
    languages = db.Column(db.String)
    vital_statistics = db.Column(db.String) 
    skin_complexion = db.Column(db.String)
    eye_colour = db.Column(db.String)
    hair_colour = db.Column(db.String)
    marital_status = db.Column(db.String)
    formal_training = db.Column(db.String)
    age_group = db.Column(db.String)
    height = db.Column(db.String)
    interested_in = db.Column(db.String)
    bio = db.Column(db.String)
    # images = db.relationship('Image', backref='user', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Model,self).__init__(**kwargs)

    def update(self, newdata):
        for key,value in newdata.items():
            setattr(self,key,value)

    def  __repr__(self):
        return '<id: %r , name:%r->'%(self.id,self.firstname + self.lastname)


"""The database model for Category"""
    __tablename__ = "categories"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False) 

    def __init__(self, name):
        self.name = name

    def  __repr__(self):
        return '<Category- id: %r - name: %r ->'%(self.id, self.name)




class Image(db.Model):
    """Image class"""
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String, nullable=False)
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('artists.id'), nullable=False)

    def __init__(self, filename, user_id):
        self.filename = filename
        self.user_id = user_id
        self.timestamp = datetime.datetime.utcnow()

    def __repr__(self):
        return '<id: %r - filename: userid: %r>'%(self.id, self.filename, self.user_id)
