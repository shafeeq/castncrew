from app import app
from models import db
db.init_app(app)

from models import Model

with app.app_context():
    db.drop_all()
    db.create_all()
    db.session.commit()


with app.app_context():
    newmodel = Model(firstname="David", lastname="Beckham", email="b@example.com", password="p",mobno="1234567890")
    db.session.add(newmodel);
    db.session.commit()
