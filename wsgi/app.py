from flask import Flask, request, render_template, redirect, url_for,g, flash, send_from_directory
import sqlite3
from flask_request_params import bind_request_params
from flask.ext.login import LoginManager
from flask.ext.login import login_user , logout_user , current_user , login_required
from werkzeug import generate_password_hash, check_password_hash
from flask.ext.seasurf import SeaSurf
import os

app = Flask(__name__)
app.config.from_object('config')

if os.environ.get('IS_THIS_PRODUCTION'):
    app.config.from_object('config-production')

app.debug = True
app.logger.info(app.config['SQLALCHEMY_DATABASE_URI'])

# bind rails like params to request.params
app.before_request(bind_request_params)

from models import User, db, Model

db.init_app(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

csrf = SeaSurf(app)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    g.user = current_user



@app.route('/', methods=['GET', 'POST'])
def index():
    app.logger.info(g.user)
    if not g.user.is_authenticated:
        return "Need a landing page here"
    model = Model.query.get(g.user.id)
    user = User.query.get(g.user.id)
    app.logger.info(model)
    app.logger.info(type(model))
    return render_template('index.html', model = model, user = user)


@app.route('/editprofile', methods=['GET', 'POST'])
@login_required
def editprofile():
    if request.method == 'GET':
        app.logger.info(repr(request.form))
        return render_template('editprofile.html')
    elif request.method == 'POST':
        app.logger.info(repr(request.form))
        params = request.params.require('model').permit('tag', 'location', 'mobilenumber', 'oneliner',
            'bio', 'hobby1', 'hobby2', 'hobby3', 'projects', 'photoshoots', 'views', 'clients')
        model = Model.query.get(g.user.id)
        model.update(params)
        db.session.add(model)
        db.session.commit()

        return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
	    app.logger.info(repr(request.form))
	    params = request.params.require('user').permit('firstname', 'lastname', 'password', 'mobno', 'email')
	    if params['password'] != request.form['confirmpassword']:
	        app.logger.info("Password mismatch")
	    	flash(' Passwords mismatch!', 'warning')
	    	return render_template("register.html")

	    app.logger.info(params.items())
	    newuser = Model(**params)
	    db.session.add(newuser)
	    db.session.commit()
	    login_user(newuser)
	    return redirect(url_for('editprofile'))

    return render_template('register.html')



@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        if g.user is not None and g.user.is_authenticated:
            return redirect(url_for('index'))
        flash('Please log in to access this page!' , 'warning')
        return render_template('login.html',page="login")

    elif request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.pwdhash, password):
                user.authenticated = True
                db.session.commit()
                login_user(user)
                return redirect(url_for('index'))
            flash('Username or Password is invalid' , 'warning')
            return render_template("login.html",page="login")
        else:
            flash('Username or Password is invalid' , 'warning')
            return render_template("login.html",page="login")


@app.route('/images/<path:filename>')
def images(filename):
    app.logger.info('filename:' + filename)
    return send_from_directory(app.config['DATA_FOLDER'], filename)


@app.route('/logout')
@login_required
def logout():
    g.user.authenticated = False
    db.session.commit()
    logout_user()
    flash('Logged out successfully.' , 'success')
    return redirect(url_for('login'))


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=5050)
